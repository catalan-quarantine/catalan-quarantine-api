package com.isadounikau.bot.catalanquarantine.service

import com.isadounikau.bot.catalanquarantine.common.GitLabProperties
import mu.KotlinLogging
import org.gitlab4j.api.GitLabApi
import javax.enterprise.context.ApplicationScoped

private val log = KotlinLogging.logger {}

interface GitLabService {
    fun triggerBranch(branch: String)
}

@ApplicationScoped
class DefaultGitLabService(
    private val gitLabApi: GitLabApi,
    private val properties: GitLabProperties
) : GitLabService {

    override fun triggerBranch(branch: String) {
        gitLabApi.pipelineApi.triggerPipeline(properties.project, properties.triggerToken, branch, listOf()).also {
            log.info { "Pipeline has been triggered [$it]" }
        }
    }
}
