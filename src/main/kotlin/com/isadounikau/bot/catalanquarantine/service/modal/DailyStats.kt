package com.isadounikau.bot.catalanquarantine.service.modal

import com.isadounikau.bot.catalanquarantine.common.Source
import java.time.LocalDate

data class DailyStats(
    val source: Source,
    val date: LocalDate,
    val epg: Long?,
    val infectedCount: Long?,
    val deadCount: Long?,
    val rt: Double?,
    val hospitalisedCount: Long?,
    val blsCount: Long?,
    val vaccinatedStats: VaccinationStats?,
)

data class VaccinationStats(
    val firstStageVaccinatedCount: Long?,
    val secondStageVaccinatedCount: Long?,
)
