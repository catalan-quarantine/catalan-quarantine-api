package com.isadounikau.bot.catalanquarantine.service.parser

import com.isadounikau.bot.catalanquarantine.service.modal.DailyStats

interface StatsParseService {
    fun getDailyStats(): DailyStats
}
