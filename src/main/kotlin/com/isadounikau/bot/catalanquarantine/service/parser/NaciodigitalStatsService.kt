package com.isadounikau.bot.catalanquarantine.service.parser

import com.isadounikau.bot.catalanquarantine.common.Source
import com.isadounikau.bot.catalanquarantine.service.modal.DailyStats
import org.jsoup.Jsoup
import org.jsoup.select.Elements
import java.time.LocalDate

class NaciodigitalStatsService : StatsParseService {

    override fun getDailyStats(): DailyStats {
        val doc = Jsoup.connect(URL).get()

        val covidDades = doc.select(".covid-dades")
        val covidDada = covidDades.select(".covid-dada")

        val epg = try {
            covidDada[5].text().toLong()
        } catch (ex: Exception) {
            0
        }
        val infected = try {
            getInt(0, covidDada)
        } catch (ex: Exception) {
            0
        }
        val dead = try {
            getInt(3, covidDada)
        } catch (ex: Exception) {
            0
        }
        val rt = try {
            covidDada[4].text().replace("(", "").replace(")", "").replace(",", ".").toDouble()
        } catch (ex: Exception) {
            0.0
        }
        val hospitalised = try {
            getInt(1, covidDada)
        } catch (ex: Exception) {
            0
        }
        val bls = try {
            getInt(2, covidDada)
        } catch (ex: Exception) {
            0
        }

        return DailyStats(
            source = Source.NACIODIGITAL,
            date = LocalDate.now(),
            epg = epg,
            infectedCount = infected,
            deadCount = dead,
            rt = rt,
            hospitalisedCount = hospitalised,
            blsCount = bls,
            vaccinatedStats = null,
        )
    }

    private fun getInt(index: Int, covidDada: Elements) = covidDada[index].text()
        .replace("(", "")
        .replace(")", "")
        .replace(".", "")
        .toLong()

    companion object {
        const val URL = "https://www.naciodigital.cat/noticia/210555/coronavirus-mapes-grafics-Catalunya-Covid-dades-evolucio-pandemia"
    }
}
