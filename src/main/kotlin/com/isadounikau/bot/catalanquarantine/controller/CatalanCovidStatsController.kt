package com.isadounikau.bot.catalanquarantine.controller

import com.isadounikau.bot.catalanquarantine.common.Source
import com.isadounikau.bot.catalanquarantine.service.DailyStatsService
import com.isadounikau.bot.catalanquarantine.service.modal.DailyStats
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter
import org.eclipse.microprofile.openapi.annotations.tags.Tag
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.concurrent.locks.ReentrantLock
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType
import kotlin.concurrent.withLock

@ApplicationScoped
@Path("/catalonia-covid-stats")
@Tag(name = "Covid Data For Catalonia Resource", description = "Basic API for data")
class CatalanCovidStatsController(
    private val dailyStatsService: DailyStatsService,
) {
    private val lock = ReentrantLock()

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getStatsTelegramMessage(
        @Parameter(example = "dadescovid") @QueryParam("source") sourceString: String,
        @Parameter(example = "1994-06-03") @QueryParam("from") fromDateString: String?,
        @Parameter(example = "2094-06-03") @QueryParam("to") toDateString: String?,
    ): HistoryStatsResponseMessages {
        val source = Source.valueOf(sourceString.toUpperCase())
        val from = fromDateString?.let { LocalDate.parse(fromDateString) } ?: LocalDate.now()
        val to = fromDateString?.let { LocalDate.parse(toDateString) } ?: LocalDate.now()

        return dailyStatsService.getDailyStats(from, to, source).map { it.toHistoryStatsResponseMessage() }.let { HistoryStatsResponseMessages(it) }
    }

    @GET
    @Path("/{source}/todaydata")
    @Produces(MediaType.APPLICATION_JSON)
    fun getTodayStats(
        @Parameter(example = "dadescovid") @PathParam("source") sourceString: String,
    ): DailyStatsResponseMessage {
        val source = Source.valueOf(sourceString.toUpperCase())
        return lock.withLock {
            val date = when (source) {
                Source.DADESCOVID -> LocalDate.now().minusDays(1) //workaround because fresh stats always for day before
                else -> LocalDate.now()
            }

            dailyStatsService.getDailyStats(date, source) ?: dailyStatsService.parseDailyStats(source)
        }.let { todayDate ->
            val yesterday = dailyStatsService.getDailyStats(todayDate.date.minusDays(1), source)

            val infectedCountDelta = if(todayDate.infectedCount != null && yesterday?.infectedCount != null) {
                todayDate.infectedCount - yesterday.infectedCount
            } else {
                null
            }

            DailyStatsResponseMessage(
                date = todayDate.date.format(DateTimeFormatter.ISO_DATE),
                epg = todayDate.epg,
                infectedCount = todayDate.infectedCount,
                infectedCountDelta = infectedCountDelta,
                deadCount = todayDate.deadCount,
                rt = todayDate.rt,
                hospitalisedCount = todayDate.hospitalisedCount,
                blsCount = todayDate.blsCount,
                firstStageVaccinatedCount = todayDate.vaccinatedStats?.firstStageVaccinatedCount,
                secondStageVaccinatedCount = todayDate.vaccinatedStats?.secondStageVaccinatedCount
            )
        }
    }
}

data class HistoryStatsResponseMessages(
    val stats: List<HistoryStatsResponseMessage>,
)

data class HistoryStatsResponseMessage(
    val date: String,
    val epg: Long?,
    val infectedCount: Long?,
    val deadCount: Long?,
    val rt: Double?,
    val hospitalisedCount: Long?,
    val blsCount: Long?,
    val firstStageVaccinatedCount: Long?,
    val secondStageVaccinatedCount: Long?,
)

fun DailyStats.toHistoryStatsResponseMessage(): HistoryStatsResponseMessage = HistoryStatsResponseMessage(
    date = date.format(DateTimeFormatter.ISO_DATE),
    epg = epg,
    infectedCount = infectedCount,
    deadCount = deadCount,
    rt = rt,
    hospitalisedCount = hospitalisedCount,
    blsCount = blsCount,
    firstStageVaccinatedCount = vaccinatedStats?.firstStageVaccinatedCount,
    secondStageVaccinatedCount = vaccinatedStats?.secondStageVaccinatedCount
)


data class DailyStatsResponseMessage(
    val date: String,
    val epg: Long?,
    val infectedCount: Long?,
    val infectedCountDelta: Long?,
    val deadCount: Long?,
    val rt: Double?,
    val hospitalisedCount: Long?,
    val blsCount: Long?,
    val firstStageVaccinatedCount: Long?,
    val secondStageVaccinatedCount: Long?,
)
