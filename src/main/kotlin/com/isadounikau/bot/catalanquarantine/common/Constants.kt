package com.isadounikau.bot.catalanquarantine.common

object Constants {
    val MESSAGE_PATTERN = """
        %s Индекс эпидем. риска EPG: %s (%s за 24 часа).
        %s Новых вирусоносителей за сутки: %s (%s за 24 часа)
        %s Индекс Rt: %s (%s за сутки).
        %s В больницах: %s (%s), из них
        %s Инт. терапия (ОИТ): %s (%s).

        ✝️ Умерших за 24 часа: %s. 
        Всего накопленным итогом: %s начала эпидемии

        Доз 💉: %s (%s)

        Целевые показатели:
        %s Индекс Rt < 1 (спад эпидемии).
        %s Rt <0,8 (контроль эпидемии).
        %s Новых вирусоносителей < 1000 в день.
        %s ОИТ < 300 больных #catstat

        Источник: открытые данные Женералитат Каталонии.
        %s

        Каталония %s
    """.trimIndent()

    val RED_ARROW_UP = "🔺"
    val BOX_ARROW_DOWN = "🔽"
    val BOX_SQUARE= "⏹"
    val RED_CROSS = "❌"
    val GREEN_CHECK_MARK = "✅"
}

