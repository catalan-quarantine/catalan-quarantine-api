package com.isadounikau.bot.catalanquarantine.common

enum class Source(
    val host: String
) {
    DADESCOVID("dadescovid.cat"),
    NACIODIGITAL("naciodigital.cat")
}
