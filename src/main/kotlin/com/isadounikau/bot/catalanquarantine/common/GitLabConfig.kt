package com.isadounikau.bot.catalanquarantine.common

import io.quarkus.arc.config.ConfigProperties
import org.gitlab4j.api.GitLabApi
import javax.enterprise.context.Dependent
import javax.enterprise.inject.Produces

@Dependent
open class GitLabConfig {

    @Produces
    fun gitLabApi(
        properties: GitLabProperties
    ): GitLabApi = with(properties) { GitLabApi(host, token) }

}

@ConfigProperties(prefix = "bot.gitlab")
open class GitLabProperties {
    lateinit var host: String
    lateinit var token: String
    lateinit var project: String
    lateinit var triggerToken: String
}
