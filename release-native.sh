./mvnw clean package -Pnative -Dquarkus.native.container-build=true
docker build -f src/main/docker/Dockerfile.native -t quarkus/hello-quarkus .
docker tag quarkus/hello-quarkus registry.heroku.com/catalan-quarantine/web
docker push registry.heroku.com/catalan-quarantine/web
heroku container:rm web -a catalan-quarantine
heroku container:release web -a catalan-quarantine
