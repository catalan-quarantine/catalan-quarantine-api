# Catalan Quarantine Data Project

Deployed to : https://catalan-quarantine.herokuapp.com/

This project uses Quarkus, the Supersonic Subatomic Java Framework.
If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

All info for datasource parsing are in the next folder `src/main/kotlin/com/isadounikau/bot/catalanquarantine/service/parser`

Daily data saved in PostgreSQL

## Setup
### Mandatory:
- Java
- Maven
- PostgreSQL

### Recommended:
- IntelliJ IDEA
- Docker

## Running the application in dev mode
You need to run Postgres before application launch
- Option One: Install Postgres locally and configure it with properties in `application.properties`
- Option Two(Recommended): Launch Docker Compose file in the project root

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```
Or you can run it from IntelliJ IDEA directly 

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `*-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/*-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.html.

